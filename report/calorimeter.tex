\section{Calorimeter}
\label{Calorimeter}

\noindent
To measure the energy of the electrons produced by the $\pi$-decay, we used a calorimeter. The calorimeter was composed of a NaI cylinder (27 cm in diameter and 33 cm in height) surrounded by a thick lead shield and connected to seven photomultiplier tubes (PMT) (see Figure \ref{PMT}). The lead shield had a circular aperture on the front (16 cm diameter). From that aperture the electrons pass through a thin aluminum shield and reach the NaI crystal (see Figure \ref{calopics}). Charged particles traversing matter leave behing a trail of excited molecules. The molecules of scintillating materials, as it is the case for the NaI crystal, release a fraction of this energy as optical photons. Thus the electrons hitting the calorimeters are completely stopped in the crystal and photons are produced. This signal is passed through to the PMTs where it is amplified.\\

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.7\textwidth]{pictures/PM.pdf}
\caption{Schematic representation of the geometric disposition of the seven PMT at the back of the calorimeter.}
\label{PMT}
\end{center}
\end{figure}

\begin{figure}[h]
\begin{center}
%\begin{subfigure}
\centering
\includegraphics[width=.6\textwidth,angle=-90]{pictures/calo1.JPG}
%\end{subfigure}%
%\begin{subfigure}
\centering
\includegraphics[width=.6\textwidth,angle=-90]{pictures/calo2.JPG}
%\end{subfigure}
\caption{Left: calorimeter viewed from behind (blue cylinder). The gray box on the left contains the high voltage generator, the seven PMT tubes can be seen on the back of the calorimeter. Right: front of the calorimeter with aluminum aperture.}
\label{calopics}
\end{center}
\end{figure}

\noindent
Before we could proceed to measure the energy of the electrons from the $\pi$-decay, we had to make sure that the calorimeter was working properly and we had to calibrate it. As a first step, we observed the signals of every PMT in order to detect possible dead or noisy channels. This was achieved by connecting one PMT at a time to a high voltage source (maximal supplied voltage: 2 kV) and by letting the voltage range from 1 to 2 kV in 0.2 kV steps. At every voltage step we read out the current flowing through the PMT and its maximum signal. The obtained data is shown in Figures \ref{pmtcurrent} and \ref{pmtmax}.\\

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{pictures/pm1.png}
\caption{Current of the PMTs as a function of the applied voltage.}
\label{pmtcurrent}
\end{center}
\end{figure}

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{pictures/pm2.eps}
\caption{Maximum signal of the PMTs as a function of the applied voltage.}
\label{pmtmax}
\end{center}
\end{figure}

\noindent
To make sure that all PMTs produced comparable signals, we chose arbitrarily one of the PMT (the first) as a reference and compared its signal with the ones from the others. This was done by discriminating the signals of PMT 1 and another photomultiplier and putting them into coincidence using coincidence units and discriminators. By relating the two input signals of the coincidence box with a logical AND, we were able to trigger only when both PMTs received a signal. By displaying the output signal of the coincidence onto an oscilloscope we were able to conclude that all the PMTs show similar signals. These different signals are shown in Figure \ref{PMTsign}.\\

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.2]{pictures/not_terminated.jpeg}
\caption{Coincidence (yellow) between the discriminated signals from two PMTs (blue and green). The analog signal from the reference PMT is shown in purple. Note that the edge of the purple signal is very irregular, due to the signal not being terminated. This error had to be corrected before taking measurements.}
\label{PMTsign}
\end{center}
\end{figure}

\noindent
The signal we received from the calorimeter had a frequency of approximatively 26 Hz (calculated with a counter) and an amplitude of about 150 mV. When we first tried to detect the activity of two radioactive sources ($^{22}$Na and $^{90}$Sr) we were not able to observe any difference in the signal with or without the sources. In fact, the signal from the redioactive sources was to little if compared with the background. The first conclusion we reached was that the calorimeter had been activated in some previous experiment and what we detected was caused by beta decays inside it. However, the real origin of the signal was found when we calculated the expected energy loss of the cosmic rays traveling through the crystal of the calorimeter.\\

\noindent
We assumed that the cosmic rays penetrate the crystal perpendicular to its surface and we knew that the minimal loss of energy in NaI is given by

\begin{equation}
\left\langle\frac{dE}{dx}\right\rangle_{min}= 1.3 \: \mathrm{MeV \: cm^2 \: g^{-1}}
\end{equation}

\noindent
Since the NaI crystal in the calorimeter is a cylinder of 27 cm diameter and 33 cm height and the density of NaI is $\rho_{NaI}=3.667 \: \mathrm{g \: cm^{-3}}$, we obtained for the energy loss:

\begin{equation}
E = 1.3 \cdot 3.667 \cdot 27 \: \mathrm{\frac{MeV \: g \: cm^2 \: cm}{g \: cm^3}} = 128.7 \: \mathrm{MeV}
\end{equation}

\noindent
The cosmic ray detection rate is roughly 1 Hz per 30 $\mathrm{cm^2}$. This means that with a crystal surface of 891 $\mathrm{cm^2}$ we expected a frequency of approximately 30 Hz. Hence, both the energy and the frequency of the signals we detected in the oscilloscope indicated that we were observing cosmic rays crossing the calorimeter. Thus, the reason why we could not detect the two radioactive sources is that we were trying to observe beta decay (with energies of a few MeV) while triggering on cosmic rays (with energies of about 150 MeV). After changing the oscilloscope trigger to 1 mV instead than 150 mV, we were finally able to detect the beta decay from both the $^{22}$Na and the $^{90}$Sr source (see Figure \ref{betadecay}).\\

\begin{figure}
\begin{center}
\includegraphics[scale=0.11]{pictures/beta_decay.JPG}
\caption{Beta decay detected with the calorimeter. The energy of the signal is of few MeV and it completely disappears once the radioactive source is removed.}
\label{betadecay}
\end{center}
\end{figure}

\noindent
Because the cosmic rays produced such a clear and stable signal, we were able to calibrate the calorimeter without using the beam. At first we tuned the PMTs by eye. That is, we kept PMT 2 as a reference and changed the voltage in the other six until the signals of all seven PMTs were approximately congruent. When the calorimeter was brought into position and all the PMTs were connected to the control room, we repeated the procedure more precisely, calculating the integral of the curve numerically. Since we detected a high number of events per second, the calculated area of every signal was stored in a histogram (see Figure \ref{hist}). To achieve similar signals from every PMT, we had to make sure that all histograms peaked at the same point. This was achieved for the voltages given in Table \ref{PMTvolt}. These voltages represent the initial setting of the calorimeter and were changed once the beam was turned on.\\


\begin{figure}
\begin{center}
\includegraphics[scale=0.4]{pictures/tof2.png}
\caption{Example of histograms of the events detected by four PMT. To calibrate the signal we adjusted the voltage of each PMT in order to achieve a value for the histogram's peak similar for every PMT.}
\label{hist}
\end{center}
\end{figure}

\noindent

\begin{table}[h]
\centering
\begin{tabular}{l|lllllll}
PM number & 1 & 2 & 3 & 4 & 5 & 6 & 7\\ \hline
Voltages [kV] & 1.5 & 2 & 1.29 & 1.6 & 1.3 & 1.37 & 1.65\\
Currents [mA] & 5 & 10 & 5 & 5 & 5 & 5 & 10\\
\end{tabular}
\caption{Voltage and current used for the seven PMTs.}
\label{PMTvolt}
\end{table}

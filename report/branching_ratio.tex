\section*{Branching ratio}

\noindent
In order to determine the branching ratio $BR = (\pi \rightarrow e) / (\pi \rightarrow \mu)$, one must obtain the total number of each decay event in a comparable kinematic and time window. The measurement was triggered whenever an electron hit the entry window of the calorimeter. Since the pion decay and the muon decay occur isotropically in all directions, the decay electrons from either the prompt pion decay or the delayed muon decay, that are actually emitted in the solid angle covered by the calorimeter should yield the same branching ratio, as if the full solid angle had been covered. The limited solid angle thus only reduces the overall number of observed events, without changing the branching ratio.\\

\noindent
The number of electrons produced via prompt decay can be extracted from the high energy region seen in \autoref{tac1cal}. By selecting only events with a TAC value between \SI{5480}{\ns} and \SI{5509}{\ns}, the electron peak can be enhanced with respect to the muon background. The background can be approximated by an exponential decay distribution, the electron peak sits on top of that in the form of a gaussian distribution. A fit as seen in \autoref{fig:e_fit} is performed. A global fit including the lower energy $\mu$ contribution was attempted, but did not produce satisfactory results.\\

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{pictures/exp_decay_fit_parts.pdf}
  \caption{Fit of the high energy and early times region of the distribution, where the electron peak is located.}
  \label{fig:e_fit}
\end{figure}

\noindent
One can then integrate the gaussian separately and obtains the number of prompt electron events to be $N_{\pi\to e} = \num{410\pm37}$.\\

\noindent
To determine the number of electrons which are produced by the subsequent decay of muons, one fits the uncut energy spectrum using the Michel distribution folded with a gaussian, to account for detector resolution effects (see \autoref{sub:energy_calibration} \vpageref{sub:energy_calibration}). For this, only the falling edge of the spectrum was used, since for lower energies, the data deviated from the expected distribution. The selected region can be seen in \autoref{fig:uncut_michel_fit_region}, the fit itself in \autoref{fig:uncut_michel_fit_result}. A global fit was attempted, but did not reproduce the region of interested as well as the limited region fit does. The remaining background above the michel edge is not described by the fit.
By integrating over the positive part of the fit, one can obtain the number of $\pi\to\mu\to e$ events to be $N_{\pi\to\mu\to e} = \num{4.714 \pm 0.161e6}$.\\

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{pictures/uncut_michel_fit_region.pdf}
  \caption{Region of the falling edge of the Michel spectrum, which was used for fitting.}
  \label{fig:uncut_michel_fit_region}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{pictures/uncut_michel_fit_result.pdf}
  \caption{Fit of the Michel distribution to the uncut energy spectrum.}
  \label{fig:uncut_michel_fit_result}
\end{figure}

\noindent
Whenever the trigger fired, a fixed width window in time was recorded by a TAC, which was calibrated as documented in \autoref{sub:tac_calibration} \vpageref{sub:tac_calibration}. The prompt decay of the stopped pion occurs within this time window with a very high probability, since its lifetime is much shorter than the width. This means that nearly all the produced prompt electrons should be contained in the TAC measurement. On the other hand, the relatively long lifetime of the muon causes a lot of muons to only decay after the TAC has reached the end of the window. Thus, only a fraction of the actually produced muons is detected.\\

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{pictures/time_fit_log.pdf}
  \caption{Time spectrum and fit, range of actual data.}
  \label{fig:time_window}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{pictures/time_fit_log_full.pdf}
  \caption{Time spectrum and fit, full range.}
  \label{fig:time_full}
\end{figure}

\noindent
To correct for this, a fit on the muon time spectrum is created as seen in \autoref{fig:time_window} and \autoref{fig:time_full}. By taking the integral from 4765 to 5520 and -10000 to 5520 respectively, one can obtain the number of muons observed in the TAC window, and the expected number of muons in total. This fit also yields a value of the $\mu$ lifetime of $\tau_\mu = \SI{2060.04 \pm 0.067}{\nano\second}$. This value deviates around $6.23\;\%$ or $2037\sigma$ from the literature value of \SI{2196.9811 \pm  0.0022}{\nano\second} \cite{PDG:2014}.


Then one can calculate the correction factor

\begin{equation}
  S = \frac{\text{total number of muons}}{\text{number of muons in window}} = \num{3.023\pm 0.659}
\end{equation}

\noindent
This means, that for every muon seen inside the TAC window, approximately \num{3.023\pm 0.659} muons were actually produced. This can then be factored in to the branching ratio as follows

\begin{equation}
  R = \frac{N_{\pi\to e}}{N_{\pi\to\mu\to e} \cdot S} = \num{2.877 \pm 0.687 e-5}
\end{equation}

\noindent
where the error was calculated by factoring in parameter uncertainties from the various fits into the integrals, and then propagating these uncertainties through to the branching ratio.\\

\noindent
For reference, one can compare this value to the one presented by Di Capua in \cite{DiCapua:1964zz}, where a value of $R_\text{lit} = \num{1.247 \pm 0.028 e-4}$ is obtained. The value determined in this experiment amounts to \SI{23.09 \pm 5.51}{\percent} of the literature value, the deviation is thus \SI{76.91}{\percent} which is about $14\sigma$.\\

\noindent
A possible reason for this deviation might be found in the lack of treatment of systematic errors. The statistical error cannot cover those. One aspect that was not accounted for is pion pileup. As described before, the mean lifetime of the muons is much longer than the readout for every time the trigger fired. The trigger rate is also too high, to have all the muons from previous events cleared from the target. This means, that muon counts we saw in an event might not have come from the $\pi$-stop that was associated with those events. This leads to an overestimation of electrons from the $\pi\to\mu\to e$ chain, and thus reduces the branching ratio.

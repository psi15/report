\section*{Pion Lifetime}

There are two ways to determine the lifetime of the pion from the obtained time spectrum. The first one uses
the decay of pions to muons and the subsequent decay of the muons to electrons. At the same time, the pions may decay directly to electrons.

\begin{equation}
  \Gamma_\text{tot} = \Gamma_{\pi \rightarrow e} + \Gamma_{\pi \rightarrow \mu}
\end{equation}

\noindent
The lifetime can then be calculated with help of the relation

\begin{equation}
  \tau = \frac{\hbar}{\Gamma_\text{tot}}.
\end{equation}

\noindent
The number of pions over time will then be given by

\begin{equation}
  \frac{\mbox{d}N_\pi}{\mbox{d}t} = - \Gamma_{\pi \rightarrow e} N_\pi(t) - \Gamma_{\pi \rightarrow \mu} N_\pi(t) = - \Gamma_\text{tot}N_\pi(t)
\end{equation}

\begin{equation}
  N_\pi(t) = N_0 e^{-\Gamma_\text{tot} t}.
\end{equation}

\noindent
Since the muons further decay into electrons, the change in the muon number will be given by

\begin{equation}
  \frac{\mbox{d}N_\mu}{\mbox{d}t} = \Gamma_{\pi \rightarrow \mu} N_\pi(t) - \Gamma_{\mu \rightarrow e} N_\mu(t) = \Gamma_{\pi \rightarrow \mu} e^{-\Gamma_\text{tot} t} - \Gamma_{\mu \rightarrow e} N_\mu(t)
\end{equation}

\noindent
Thus the total number of muons is (assuming that initially there are no muons present)

\begin{equation}
  N_\mu(t) = k_1 (e^{-\Gamma_{\mu \rightarrow e} t} - e^{-\Gamma_\text{tot} t})
\end{equation}

\noindent
where $k_1$ is a constant. We can now calculate the change in the electron number where we neglect the electrons produced directly in the $\pi \rightarrow e$ decay since this process takes place on a timescale that is about 100 times shorter than the $\mu \rightarrow e$ decay.

\begin{equation}
  \frac{\mbox{d}N_e}{\mbox{d}t} = \Gamma_{\mu \rightarrow e} N_\mu(t) = k_2 (e^{-\Gamma_{\mu \rightarrow e} t} - e^{-\Gamma_\text{tot} t})
  \label{numberelectrons}
\end{equation}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=\textwidth]{pictures/tac1CalNoCuts.pdf}
  \end{center}
  \caption{Number of electrons at given energy and time. The red lines represent the cuts to enhance the prompt electrons.}
  \label{tac1cal}
\end{figure}

\noindent
\autoref{tac1cal} shows the data that was used for the analysis. It shows the number of electrons detected at given energies and times. From the data we can get the number of electrons detected at any given time and make an exponential fit using the function in equation \ref{numberelectrons}. This allows us to determine the total decay rate $\Gamma_\text{tot}$. The data together with the exponential fit can be seen in \autoref{lifetime_pi_mu_e}.\\

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=\textwidth]{pictures/lifetime_fit.pdf}
  \end{center}
  \caption{Decay via muons: Number of electrons detected against inverted time after $\pi$-stop with exponential fit}
  \label{lifetime_pi_mu_e}
\end{figure}

\noindent
From the exponential fit the pion lifetime was found to be
\begin{equation}
  \tau_\pi = \frac{\hbar}{\Gamma_\text{tot}} = \SI{26.239 \pm 0.256}{\nano\second}
\end{equation}

\noindent
The other method to obtain the lifetime is to use the electrons produced via prompt decay of the pions. This should be preferential and yield
a better value, since only one statistical decay process affects the observed distribution. In order to enhance the prompt electrons, one can select
events where the energy lies inside a window from \SI{76}{\MeV} to \SI{80}{\MeV} (see \autoref{tac1cal}).\\

\noindent
The lifetime can be obtained from this distribution by fitting the following function:

\begin{equation}
  f(t) = A\cdot e^{-(T-t)/\tau_\pi} + bx + c
\end{equation}

\noindent
where the parameter $T$ was introduced to shift the exponential function to the correct position, and $t$ has a negative sign, since time measurements
were flipped. Additionally a linear background is assumed. The fit can be seen in \autoref{lifetime_pi_e}. $\tau_\pi$ yields the pion lifetime. This value can be extracted from the fit to be \SI{26.352 \pm 6.865}{\ns}.\\

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=\textwidth]{pictures/htimePiE.pdf}
  \end{center}
  \caption{Prompt decay of $\pi$ to $e$: Number of electrons detected against inverted time after $\pi$-stop with exponential fit.}
  \label{lifetime_pi_e}
\end{figure}

\noindent
The literature value for the lifetime of the pion is $\tau_\text{lit} = \SI{26.033 \pm 0.005}{\nano\second}$ as found in \cite{PDG:2014}. Thus, the lifetime value obtained from the $\pi \to \mu \to e$ chain differs from the literature value by $0.79\%$, and the $\pi\to e$ chain value $1.23\%$ respectively. The uncertainty of the value obtained from the $\pi\to e$ chain is rather high, which stems from the fact that the data set after selection only contains a couple of hundreds of events.

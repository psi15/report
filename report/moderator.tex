\section{Moderator}
% Can you delete all the captions above the figures and put them in the latex captions below? Also they always want all the axis units and axis numbers to be large enough, would be great if you could increase the size.
% Maybe add through what process the pions lose energy when passing through the moderator
\noindent
In order to stop the relativistic pions in the target, a plastic moderator was placed in front of the beam, significantly reducing the pions' velocities and energies. We calculate the thickness the moderator should have in order to maximize the pion stop rate in the target. The average energy loss of charged particles with charge $z$, velocity $v$ and energy $E$ passing through a homogeneous medium is governed by the Bethe-Bloch formula \cite{Bethe:1953}:

\begin{equation}
- \left\langle \frac{dE}{dx} \right\rangle = \frac{4 \pi n z^2}{m_e c^2 \beta^2} \left( \frac{e^2}{4 \pi \epsilon_0} \right)^2 \left[ \ln \left( \frac{2m_e c^2 \beta^2}{I \cdot (1 - \beta^2)} \right) - \beta^2 \right]
\label{bethe}
\end{equation}

\noindent
where $\beta = \frac{v}{c}$, $Z$ is the charge number of the moderator material, $I \simeq (10 \: eV) \times Z$ is the mean excitation potential, and $n$ is the electron density of the material given by:

\begin{equation}
n = \frac{N_A Z \rho}{A M_u}
\end{equation}

\noindent
where $N_A$ is Avogadro's constant, $\rho$ is the density of the material, $A$ is the atomic mass number and $M_u$ is the molar mass constant.\\

\noindent
We are interested in the behavior of the average pion which has an initial energy $E_{init} = \sqrt{p_{init}^2 + m_{\pi}^2}$, where $p_{init}$ is the initial momentum which could be adjusted in our setup. We note that in the experiment, most variables in Equation \ref{bethe} are constant and we define:

\begin{equation}
C_1 = \frac{4 \pi n z^2}{m_e c^2} \left( \frac{e^2}{4 \pi \epsilon_0} \right)^2, \: C_2 = \frac{2m_e c^2}{I}
\end{equation}

\noindent
Then, Equation \ref{bethe} assumes the following form:

\begin{equation}
\frac{dE}{dx} = - C_1 \left[ \beta^{-2} \ln \left( C_2 \frac{\beta^{2}}{(1-\beta^2)} \right) - 1 \right]
\label{bethe2}
\end{equation}

\noindent
where $\beta$ is related to the energy $E$ through:

\begin{equation}
E = \gamma m_{\pi} c^2 = \frac{m_{\pi} c^2}{\sqrt{1 - \beta^2}} \Longleftrightarrow \beta = \sqrt{1 - \frac{m_{\pi}^2 c^4}{E^2}}
\label{beta}
\end{equation}

\noindent
Putting the expression from Equation \ref{beta} into Equation \ref{bethe2}, we get:


\begin{equation}
\frac{dE}{dx} = - C_1 \left[ \frac{1}{1 - \frac{m_{\pi}^2 c^2}{E^2}} \ln \left( C_2 (1 - \frac{m_{\pi}^2 c^4}{E^2}) \frac{E^2}{m_{\pi}^2 c^4} \right) - 1 \right]
\label{bethe3}
\end{equation}

\noindent
Splitting the logarithm into three terms, putting all the $E$ dependent terms on the left hand side and integrating over $dx$ on both sides leads to the moderator distance $d_{moderator}$ needed to slow down a pion from the initial energy $E_{init}$ to the final energy $E_{fin}$:

\begin{multline}
d_{moderator} = \int_{x_{init}}^{x_{fin}} dx = - \int_{E_{init}}^{E_{fin}} C_1^{-1} \times\\
\left[ \frac{1}{1 - \frac{m_{\pi}^2 c^2}{E^2}} \left( \ln (C_2) + \ln \left( 1 - \frac{m_{\pi}^2 c^4}{E^2} \right) + \ln \left( \frac{E^2}{m_{\pi}^2 c^4} \right) \right) - 1 \right]^{-1} dE
\label{bethe4}
\end{multline}

\noindent
In order to bring the pions to a complete stop in the target, the kinetic energy should be reduced to a negligible fraction of the pion's rest mass. Thus we get:

\begin{equation}
E_{fin} = \frac{m_{\pi} c^2}{\sqrt{1 - \beta_{fin}^2}} = m_{\pi} c^2 + \mathcal{O}(v_{fin}^2)
\end{equation}

% The caption is pretty long, can you shorten it or put some of it in the main text?
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.8\textwidth]{pictures/Moderator2.pdf}
\end{center}
\caption{Average distance needed to stop pions with initial momentum $p$. The solution was obtained solving Equation \ref{bethe4} numerically. For low pion momenta (below $p = 50$ MeV), the moderator distance needed to stop the pions is close to zero. As the initial momentum increases, the average distance to stop the pions increases as well. We note that for a pion beam with initial momentum $p = 150$ MeV, a moderator of $d = 15$ cm suffices to stop the pions.}
  \label{moderator2}
\end{figure}

\noindent
As Equation \ref{bethe4} has no obvious analytical solution, we performed a numerical integration using an integration scheme with a centered rectangular lattice. The result is presented in Figure \ref{moderator2}.\\

% better check if what I've written here is correct
\noindent
Two possible beam momenta can be chosen according to the calculations of the time of flight of the decay products: $p_1 = 150$ MeV and $p_2 = 225 $ MeV. At higher momenta, we would require a much greater moderator thickness. While the pions lose energy in the moderator, they are also scattered. The central 98\% of the scattering angle distribution can be approximated by a Gaussian distribution with a variance given by $\theta_0$ \cite{PDG:2014}:

\begin{equation}
\theta_0(d_{mod}) = \frac{13.6 \mathrm{MeV}}{\beta c p} z \sqrt{\frac{d_{mod}}{X_0}} \left( 1 + 0.038 \ln \left( \frac{d_{mod}}{X_0} \right) \right)
\end{equation}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{pictures/ScatteringAngle.pdf}
  \end{center}
  \caption{Variance of the scattering angle of the pions exiting the moderator with the moderator distance adapted according to figure \ref{moderator2}.}
 \label{Scattering}
\end{figure}

% can you add what material we used for the moderator, either here or in the beginning of the section
\noindent
where $X_0$ is the radiation length of the material (in our case: $X_0 \simeq 0.3$ m). In Figure \ref{Scattering} the variance of the scattering angle of the pions coming out of the moderator with the moderator distance adjusted to stop the pions of a given momentum is shown. We note that for small pion momenta the scattering angle is low and reaches a maximum at $\theta_0(p_1 = 150 \mathrm{MeV}) \simeq 4.9 ^{\circ}$. At $p_2 = 225$ MeV, the variance of the scattering angle of the pions is roughly the same, with $\theta_0(p_2) \simeq 4.8 ^{\circ}$. This result indicates that increasing the beam momentum does not increase the variance of the scattering angle if the moderator distance is adjusted to stop the pions in the target.\\

\noindent
Increasing the beam momentum from $p_1$ to $p_2$ would increase the pion decay rate by an order of magnitude and thus shorten measurement times. However, our DAQ (data acquisition) and experimental setup was the limiting factor for how much data we could collect. A beam with momenta of $p_1 = 150$ MeV maximized the data we could collect with our DAQ, although in principle one would collect more data with a beam of momentum $p_2$.\\

\noindent
After obtaining the theoretical estimation for the appropriate moderator length for pion momenta of $p_1 = 150$ MeV, we measured the dependence of the number of pions stopped as a function of the moderator length. This was achieved using a moderator with an adjustable length that was placed in front of the target. The length could be varied in steps of 5 mm in the region $d_{mod} \in [0,0.20]$ m. We expected a very low $\pi$-stop rate for small $d_{mod} < 0.15$ m, as most of the pions would pass through both the target and the veto. As $d_{mod}$ was increased and reached $0.15$ m, we expected this rate to increase and reach a maximum. On the other hand, for $d_{mod} > 0.15$ m we expected the rate to decrease, as most of the pions would be completely stopped in the moderator. The result obtained is presented in Figure \ref{strings}. We see a sharp maximum of the pion stop rate at $d_{moderator} \simeq 0.105$ m. The difference with the theoretical value of $d_{mod} = 0.15$ m can be partly explained by the non-negligible thickness of the first two Scintillators which were measured to be $d_{Sc} = d_{Sc1} + d_{Sc1.1} = 0.014$ m. Adding this contribution to the moderator distance brings us closer to the theoretical value: $d_{moderator} + d_{Sc} = 0.119$ m. Additional uncertainties come from neglecting interactions of the pions with the air and approximating the moderator as a continuous medium of 0.15 m length, which actually consisted of several pieces with thicknesses between 0.0025-0.01 m put in a row along the beam line.\\

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.8\textwidth]{pictures/Strings.pdf}
\end{center}
\caption{Rate of the pion stop as a function of the moderator distance.}
\label{strings}
\end{figure}



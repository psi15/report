\section{Scintillators}

\noindent
A scintillator consists of a material that emits visible light when ionizing particles pass through. When passing through the material, the particles excite the electrons inside it through inelastic scattering processes. Once these excited electrons relax back into the lower energy level, a photon with a characteristic wavelength, depending on the energy gap between the involved energy levels, is emitted. The photons that are emitted by the primary material of the scintillator will have the same wavelength that is necessary to excite the surrounding atoms, which results in a high reabsorption rate for these photons. Only the photons that are emitted by impurities in the material will have a shifted wavelength, that can then be detected by a photon detector. This detector is connected to the scintillating material via a light guide. The shift in wavelength occurs, because each atom has a characteristic energy gap between the different energy levels of their electrons. This gap highly depends on the amount of electrons that are present in the shell and the initial and final electron state involved in the absorption process. The photon detector used in this experiment was a photomultiplier tube (PMT). A PMT works by converting the incident photon into primary electrons at a photocathode via the photoelectric effect. The ejected electrons are then focused and accelerated along multiple dynodes towards the anode. Due to the acceleration process, the number of electrons will increase, depending on the amount of voltage applied for the acceleration. The amplification of a PMT can be as high as a million times, allowing for the detection of even single photons if the incidence flux is very low.\\

\noindent
In the experiment we used plastic scintillators (Bicron BC-404, \cite{bc40045:online}) which have a density of 1.032 $\mathrm{g}/\mathrm{cm}^3$, a maximal emission at 408 nm and a decay time of only 1.8 ns which makes them well suited for fast timing measurements.

\section*{Calibration}

\noindent
One of the first steps to take is to determine the response of the scintillators and how well-shaped their signals are. This was done using a radioactive source and connecting the PMT to an oscilloscope to analyze the resulting signal. An example of the resulting plateau measurement is shown in Figure \ref{plateau}. The applied voltage influences the signal created in the PMT immensely. This is due to the amplification process, which is based on the acceleration of electrons by the applied voltage. The higher the voltage, the more electrons are ejected on the impact of a single electron on the surface of a dynode. At some point, the number of secondary electrons per incident electron will reach a saturation point, as the energy of the electrons will be set between two different energy levels, and further increase of the voltage in small steps will not allow the excitation of more electrons until the next higher energy level is reached. Beyond this effect, at some point, the accelerated electrons will be able to ionize the surrounding gas in the PMT, which will result in a breakthrough current through the gas. If this occurs, there is a high risk of damaging the scintillator. Because of this, the applied voltage should not succeed a certain amount, that can vary depending on the PMT. Additionally, at too high voltages the noise will be amplified so much, that it can no longer be discerned from the actual signal. The high voltage applied to the PMT during these measurements was slowly increased in steps of $\leq$100 V in order to determine the optimal working point where the signal-to-noise ratio was the highest.\\

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{pictures/plateau.png}
\caption{Frequency of registered events versus applied voltage at the PMT.}
\label{plateau}
\end{center}
\end{figure}

\noindent
The scintillators used for time measurements need to show a sharp rise in their signals to reduce the delay between the signal starting to rise and the time it passes the threshold for registration. Additionally, they should be rather small as the time travelled inside the light guide and PMT is of significant order for large modules. 
On the other hand, scintillators used as a veto should have a large  area to cover enough angles and to have very high efficiency and low signal-to-noise ratio in order to reduce the number of events that are vetoed mistakenly. Keeping this in mind and using the previously gathered information, a list (shown in Figure \ref{sizes}) of potential candidates for the time measurement, as well as for the veto candidates was created.  For these, a noise-measurement (without radioactive source) and a plateau-measurement (with radioactive source) were conducted in order to determine a good working point for these scintillators in the final set-up.\\

\begin{figure}[!htb]
\begin{center}
  \begin{tabular}{crrrrr}
    \toprule
    Scintillator & width [cm] & length [cm] & thickness [cm]\\ \midrule 
    a & 10,2 & 10,2 & 0,6\\ \midrule
    b & 11,9 & 16,0 & 0,6\\ \midrule
    c & 5,3 & 6,2 & 1,2\\ \midrule
    f & 18,0 & 18,8 & 0,4\\ \midrule
    h & 25,0 & 36,3 & 2,0\\ \midrule
    i & 30,1 & 35,0 & 0,4\\ \midrule
    j & 30,0 & 41,5 & 0,5\\ \midrule
    k & 6,9 & 27,5 & 0,6\\ \midrule
    l & 5,2 & 15,0 & 0,4\\ \midrule
    m & 5,2 & 14,0 & 0,4\\
    \bottomrule
  \end{tabular}
\end{center}
\caption{List of scintillators that were considered as veto or time measurement candidates and there corresponding sizes.}
\label{sizes}
\end{figure}

\section*{Efficiency Measurement}

\noindent
In order to determine the efficiency of the scintillators, one has to set up two reference scintillators $A$ and $C$ encasing the scintillator $B$ one intends to measure. An important fact to consider is the geometry of this set-up, as the area overlap of both reference scintillators $A$ and $C$ has to be smaller than the area of the measured scintillator $B$ and needs to overlap with it as well. A sketch of the setup can be seen in Figure \ref{efficiency}. Once the geometrical set-up is complete, the two reference scintillators' signals are put into coincidence and the number $N_{AC}$ of these coincidences is counted for a fixed time period. Additionally the coincidence $N_{ABC}$ between all three scintillators' signals is also counted. The efficiency of scintillator $B$ in relation to the reference scintillators is then calculated by:

\begin{figure}[h]
  \centering
  \resizebox{0.7\linewidth}{!}{
    \input{scintillator_efficiency.tikz}
  }
  \caption{Sketch of geometrical setup necessary for smallest scintillators.}
  \label{efficiency}
\end{figure}

\begin{equation}
\mathrm{Eff}_B = \frac{N_{ABC}}{N_{AC}}
\end{equation}

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{pictures/efficiency.png}
\caption{Measurement of the efficiency curve for one scintillator.}
\label{efficiency-measure}
\end{center}
\end{figure}

\noindent
This measurement is then performed at varying high voltages for scintillator $B$ in order to determine the efficiency curves. This procedure is repeated until all scintillators have been tested. The resulting efficiencies are summarized in Figure \ref{Efftable}.\\

\begin{figure}[!htb]
\begin{center}
  \begin{tabular}{crrrrr}
    \toprule
    Scintillator & Voltage [V] & Efficiency [\%]\\ \midrule 
    a & 2150 & 98,0 \\ \midrule
    b & 2150 & 98,1 \\ \midrule
    c & 1700 & 99,2 \\ \midrule
    f & 2200 & 98,2 \\ \midrule
    h & 2150 & 99,86 \\ \midrule
    i & 2150 & 99,8 \\ \midrule
    j & 2300 & 99,96 \\ \midrule
    k & 1900 & 99,8 \\ \midrule
    l & 2050 & 94,4 \\ \midrule
    m & 2000 & 99,8 \\
    \bottomrule
  \end{tabular}
\end{center}
\caption{List of applied voltage and corresponding the efficiencies measured for different scintillators.}
\label{Efftable}
\end{figure}

\noindent
During this experiment, all scintillators were set-up in the beam line in a fitting geometry in order to determine all but the two out-most scintillators' efficiencies without the need to change the set-up multiple times. Only the trigger logic in the control room had to be changed. The two smallest scintillators were then measured separately using the geometrical overlap of larger scintillators as shown in Figure \ref{efficiency}.\\

\section*{Energy Calibration}
\label{sub:energy_calibration}

\noindent
The electrons produced directly in the pion decay are Gaussian distributed peaking at approximatively 70 MeV, since

\begin{equation}
 \frac{m_{\pi}-m_e}{2} \sim \frac{m_{\pi}}{2}= \frac{139.6}{2} \; \mathrm{[MeV/c^2]} = 69.8 \; \mathrm{[MeV/c^2]}
\end{equation}

\noindent
As previously mentioned, the energy measurement of the electrons is done by seven PMT's in the NaI calorimeter. The sum over the integrated signal of the seven PMT's is considered for every run. A distribution of events as a function of a quantity proportional to the energy is obtained for each run. We calibrated the energy using the following strategy:\\

\noindent
The Michel spectrum fitting function is obtained by convoluting two functions:\\

\noindent
1) a third order approximation of the electron energy distribution $\mathrm{d} \Gamma/ \mathrm{d} x$ integrated over the polar angle, depending on the energy $E$ and the three parameters $a$,$b, c \in \mathbb{R}$:

\begin{equation}
\frac{\mathrm{d} \Gamma}{\mathrm{d} x}(E) \simeq 3 a \left(\frac{E}{c} \right)^2 - 2 b \left(\frac{E}{c}\right)^3
\end{equation}

\noindent
2) the calorimeter resolution $\hbox{Res}(E)$ expressed as a Gaussian function depending on the energy $E$ and the three parameters $ A, \mu, \sigma \in \mathbb{R}$:

\begin{equation}
\hbox{Res}(E) = A \cdot \hbox{Exp} \left( - \frac{(\mu-E)^2}{2 \sigma^2}\right)
\end{equation}

\noindent
The convolution of these two function gives the fitting function $f(E)$:

\begin{equation}\label{MichelFit}
\begin{split}
  f(E)   & = \int_{\mathbb{R}} \mathrm{d}E' \frac{\mathrm{d} \Gamma}{\mathrm{d} x}(E-E') \cdot \hbox{Res}(E') \\&= \frac{A \sqrt{2 \pi} \sigma}{c^3} \left[(-2 b(E -\mu)^3 + 6 b (-E + \mu) \sigma^2 + 3 a c ((E-\mu)^2 + \sigma)^2 \right]\end{split}
\end{equation}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{pictures/calSumHist_003522.pdf}
    \caption{Histogram with the events of run 3522. The convolution of the Michel function with a Gaussian ($f(E)$, see \autoref{MichelFit}) is used to fit the data. The data is fitted only in the interval $E \in[10,180] \cdot 10^3$ (arbitrary units), since the rise at low energy is not described by the fit function.}
    \label{fig_Uncalibrated}
  \end{center}
\end{figure}

\noindent
This function $f(E)$ reproduces the shape of the Michel spectrum. One fits the data to this function for every run in order to get rid of time effects, such as drifts in calorimeter calibration. We assume that the origin of the Michel fit corresponds to $E_1 = 0$ MeV. The x-axis value of the maximum of the Michel spectrum corresponds to $E_2 = \SI{52.829}{\MeV}$ (half the rest mass of the muon). One uses these two points to rescale the spectrum and get the calibrated energy spectrum.\\

\noindent
We fit the data with $f(E)$ over a reasonable energy range as can be seen in \autoref{fig_Uncalibrated}. It is reasonable to neglect the low energy points which are overestimated because of the noise introduced by the calorimeter and natural to stop the Energy fit values slightly above the visible Michel edge. Using this method, one obtains the six parameters $a,b,c,A,\sigma, \mu$ for each run and one can calculate the position of the maximum of the Michel spectrum. The zero energy point is naturally obtained by the intersection of the function with the x axis. Using these two points, one can rescale the energy axis and obtain the number of events represented as a function of the calibrated energy for each run.
Five runs (3528, 3573, 3729, 3859, 4105) with 87094 events in total where the Michel position determined from the automated fit was more than \num{20e3} away from the expected position of \num{120e3} were rejected and not used for any subsequent analysis. A total of 302 runs containing 5257984 events were used for further analysis. The resulting Michel position over the run number and the $\chi^2_\text{red}$ values over run numbers can be seen in \autoref{fig:michel_over_runs} and \autoref{fig:chi2ndf_over_runs} respectively.
%The result is presented in \autoref{Calibrated_Spectrum}.
One can then sum up all the runs and get the energy distribution of the experiment. The result is presented in \autoref{Calibrated_E_Sum_Run}.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{pictures/michel_fits_over_runs.pdf}
    \caption{Michel edge position over run number}
    \label{fig:michel_over_runs}
  \end{center}
\end{figure}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{pictures/chi2_ndof_over_runs.pdf}
    \caption{$\chi^2_\text{red}$ over run number}
    \label{fig:chi2ndf_over_runs}
  \end{center}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{pictures/calSumHistNoCuts.pdf}
  \caption{Total energy distribution, with all runs summed up.}
  \label{Calibrated_E_Sum_Run}
\end{figure}

\section{Introduction}

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{./pictures/cyclotron.png}
\end{center}
\caption{Isochronous Ring cyclotron at PSI capable of accelerating protons to 590 MeV.}
\label{cyclotron}
\end{figure}

\noindent
{The pion was first predicted in 1935 by Yukawa \cite{Yukawa:1935xg} as the carrier particle of the strong force within atomic nuclei. He subsequently received the Nobel prize in physics in 1949 after its discovery. Due to the short range of the nuclear force, deduced from the size of the nucleus, the mass of this carrier particle was predicted to be around 100 MeV. Together with the prediction of this carrier particle Yukawa also introduced the characteristic "Yukawa potential" which such an interaction would have.\\

\noindent
Charged pions originating from cosmic rays were first detected in 1947 by Powell, Lattes and Occhialini using the photographic emulsion technique (\cite{Lattes:1947mx}, \cite{Lattes:1947my}). Subsequently pion decays were studied using photographic plates and it was revealed that muons were one of the decay products. As the neutral pion could not be detected using the same techniques, it was until 1950 that it was discovered at the University of California's cyclotron. Its existence was inferred from the two photons the $\pi^0$ decays into \cite{Burfening:1987na}.\\

\noindent
In 1933 Fermi introduced his interaction theory to describe the beta decay of nuclei. The interaction is described with four-fermion vertices. It is a valid approximation for the weak interactions at energies much lower than the mass of the intermediate vector bosons, however, parity violation is not taken into account in it. Both Marshak and Sudarshan \cite{Sudarshan:1958vf} and Feynman and Gell-Mann \cite{Feynman:1958ty} subsequently proposed the V-A theory in 1957, according to which the weak interaction only acts on left-handed particles and right-handed antiparticles which explains the maximal violation of parity.\\

\noindent
In this experiment the decay of the $\pi^+$ particle is studied, the lightest, charged meson comprised of an up and an antidown quark with a lifetime of $\tau = \SI{26.033 \pm 0.005}{\nano\second}$ \cite{PDG:2014}. The charged pions decay into leptons - muons (antimuons) and electrons (positrons) - although the probability of the electron decay mode is four orders of magnitude smaller. $\beta$-pion decay has also been observed, where the neutral pion and an electron (positron) are emitted from a charged one. The neutral pion mostly decays into a pair of photons. It is the goal of this experiment to find the branching ratio of the two primary decay modes of the charged pion.\\

\noindent
The weak theory predicts the following two leading order decay modes for the pion:

\begin{equation}
\pi^+ \rightarrow \mu^+ + \nu_\mu
\end{equation}

\begin{equation}
\pi^+ \rightarrow e^+ + \nu_e
\end{equation}

\noindent
These can also be seen in Figure \ref{pion decay}.\\

\noindent
The Standard Model of particle physics is a theory that describes the elementary particles of matter and their interactions \cite{Griffiths:2011}. There are two types of particles: six quarks and six leptons (and their antiparticles). The gauge bosons mediate the interactions between these particles. The gluon is the force carrier of the strong force, the photon is the force carrier of the electromagnetic force and the W$^+$, W$^-$ and Z$^0$ bosons are the force carriers of the weak force, which mediate the transfer of momentum, spin and energy. The radiative decay mode $\pi^+ \rightarrow \mu^+ \nu_{\mu} \gamma$ has a slightly larger branching ratio than the direct decay to positrons but could not be distinguished in the experiment. The pion has spin 0, so the leptons and lepton neutrinos have to be emitted with opposite spins and momenta in the rest frame of the pion. As only left handed neutrinos are encountered in nature, the leptons must have spins pointing in the direction of their momenta, thus being left handed. If leptons were massless they would only be found as left handed particles, that is the $e^+$ and $\mu^+$ would have to be right handed. In this case however, the previously mentioned decays would be prohibited. Due to the small mass of the electron, compared to the one for muon, the electronic decay is therefore suppressed.\\

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}[node distance=2cm and 2cm]
\coordinate[label=left:$\nu_\mu (\nu_e)$] (p1);
\coordinate[below right=of p1] (node1);
\coordinate[above right=of node1,label=right:$\mu^+ (e^+$)] (p2);
\coordinate[below=2cm of node1] (node2);
\coordinate[below left=of node2,label=left:$u$] (p3);
\coordinate[below right=of node2,label=right:$\overline{d}$] (p4);

\draw[particle] (node1) -- (p1);
\draw[particle] (p2) -- (node1);
\draw[boson] (node1) -- node[label=right:$W^+$]{} (node2);
\draw[particle] (node2) -- (p3);
\draw[particle] (p4) -- (node2);
\end{tikzpicture}
\end{center}
\caption{The $\pi+$ decays into a $\mu^+$ and a $\nu_\mu$ ($e^+$ and a $\nu_e$).}
\label{pion decay}
\end{figure}

\noindent
The $\pi e / \pi \mu$ branching ratio predicted by theory is given by:

\begin{equation}
R = \frac{m_e^2}{m_\mu^2} \left( \frac{m_\pi^2 - m_e^2}{m_\pi^2 - m_\mu^2} \right)^2 = 1.283 \times 10^{-4}
\end{equation}

\noindent
The experimental confirmation of the theoretical branching ratio provides a test for the V-A theory.\\

\noindent
In 1963 Di Capua, Garland, Pondrom and Strelzoff conducted an experiment to measure the pion branching ratio $\left( \pi e / \pi \mu \right)$ with an accuracy of 2\% to confirm the results given by the universal theory for weak couplings \cite{DiCapua:1964zz}. They used a pion beam with a momentum of 145 MeV which passed through a moderator and was stopped in a target. The pions decayed into muons or electrons, the muons subsequently decayed into electrons. The energy of the decay electrons was measured with a NaI(Tl) calorimeter. As the electrons resulting from the muon decay and the ones emitted by the pions have different energies and arrived at the calorimeter at different times due to the different lifetimes, the two decay modes could be distinguished and an accurate measurement of the branching ratio was obtained. This value of $BR = (1.247 \pm 0.028) \times 10^{-4}$ \cite{PDG:2014} is in excellent agreement with the theoretical value after taking into account radiative corrections thus substantiating the universality of the weak force and confirming the equal coupling constants between $\left( e, \: \nu_e \right)$ and $\left( \mu, \: \nu_\mu \right)$ in the theory of weak interactions.\\

\noindent
Due to positive pions and anti-muons both decaying into positrons, the two signals have to be distinguished. In the experiment of Di Capua et al. this was achieved by measuring the times between the pion stop and the arrival of the positrons and by comparing the two different energies. Due to the two orders of magnitude difference in the life times of the pion and muon, positrons from a pion arrive earlier and also have a larger maximum energy since the pion only decays into two particles $\left( e, \: \nu_e \right)$. We will distinguish the two types of electrons through their energies and the use of TACs (time to amplitude converter).\\

\noindent
For the acceleration of the protons used to create the pion beam in this experiment, the Injector 2 and the Ring cyclotron at PSI were used. The Injector 2 is comprised of an ion source emitting protons with 60 keV, a Cockcroft-Walton accelerator which is capable of producing a voltage of up to 810 kV and an isochronous cyclotron that operates using varying magnetic field strengths depending on the radius. The great advantage of this type of cyclotron is the capability of producing much higher beam currents compared to conventional ones or synchrocyclotrons. The cyclotron in Injector 2 is comprised of four magnets and has a frequency of 50 MHz. The 870 keV protons are further accelerated to 72 MeV. They are then fed into the Ring cyclotron, see Figure \ref{cyclotron}. The Ring is another isochronous cyclotron consisting of 8 magnets with a frequency of 50 MHz. Here the protons are further accelerated to an energy of 590 MeV and then guided to graphite production targets. The pions for our experiment are created from the high energy protons hitting the graphite targets and thus producing smaller subatomic particles, mainly pions.\\

\noindent
The energy spectrum of the positron in the $\pi e$ decay has a sharp peak at $E = \frac{m_{\pi}}{2} = 69.8$ MeV  as we have a two body decay and the positron and neutrino are emitted back to back with the same momentum in the rest frame of the pion. The $\mu^+$ decay is a three body decay, with the muon emitting a positron and two neutrinos:

\begin{equation}
\mu^+ \rightarrow e^+ + \overline{\nu}_\mu + \nu_e
\end{equation}

\noindent
The corresponding Feynman diagram is given in Figure \ref{muon decay}. Thus the energy of the positron is spread out over a much broader range due to the distribution of momenta over the three particles. The energy spectrum consists of a constant rise from an energy of 0 to the maximum energy of $E = \frac{m_{\mu}}{2} = 52.8$ MeV and a sharp drop at energies greater than half the muon mass, see Figure \ref{michel}. This distribution is known as the Michel spectrum \cite{Michel:1949qe}.\\

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}[node distance=2cm and 1cm]
\coordinate[label=left:$\mu^+$] (p1);
\coordinate[above right=of p1] (node1);
\coordinate[above right=0.5cm and 1cm of node1] (node2);
\coordinate[above left=of node1,label=left:$\overline{\nu}_\mu$] (p2);
\coordinate[above right=1.5cm and 0.5cm of node2,label=left:$e^+$] (p3);
\coordinate[above right=1.5cm and 2cm of node2,label=right:$\nu_e$] (p4);

\draw[particle] (node1) -- (p1);
\draw[boson] (node1) -- node[label=below right:$W^+$]{} (node2);
\draw[particle] (p2) -- (node1);
\draw[particle] (p3) -- (node2);
\draw[particle] (node2) -- (p4);
\end{tikzpicture}
\end{center}
\caption{The $\mu^+$ decays into a $e^+$, a $\overline{\nu}_\mu$ and an $\nu_e$.}
\label{muon decay}
\end{figure}

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.75\textwidth]{./pictures/michel.png}
\end{center}
\caption{Muon decay energy spectrum (Michel spectrum) with and without radiative corrections.}
\label{michel}
\end{figure}
